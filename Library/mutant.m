KRP_SI=true; SMR_SI=true;eq_inh=[];

CellDataCDKAOE=MultiCellModel(KRP_SI,SMR_SI,eq_inh,1.5, 1);
CellDataCDKAWT=MultiCellModel(KRP_SI,SMR_SI,eq_inh,1.0, 1);
CellDataCDKAUE=MultiCellModel(KRP_SI,SMR_SI,eq_inh,0.8, 1);


CellData=CellDataCDKAOE;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CA_OE=DivisionSizes;
Time_G1S_CA_OE=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CA_OE=DivisionSizes;
Time_Div_CA_OE=tCellCycle;



CellData=CellDataCDKAWT;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CA_WT=DivisionSizes;
Time_G1S_CA_WT=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CA_WT=DivisionSizes;
Time_Div_CA_WT=tCellCycle;






CellData=CellDataCDKAUE;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CA_UE=DivisionSizes;
Time_G1S_CA_UE=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CA_UE=DivisionSizes;
Time_Div_CA_UE=tCellCycle;

%%
%CDKB

KRP_SI=true; SMR_SI=true;eq_inh=[];

CellDataCDKBOE=MultiCellModel(KRP_SI,SMR_SI,eq_inh,1, 1.5);
CellDataCDKBWT=MultiCellModel(KRP_SI,SMR_SI,eq_inh,1, 1.0);
CellDataCDKBUE=MultiCellModel(KRP_SI,SMR_SI,eq_inh,1, 0.8);


CellData=CellDataCDKBOE;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CB_OE=DivisionSizes;
Time_G1S_CB_OE=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CB_OE=DivisionSizes;
Time_Div_CB_OE=tCellCycle;



CellData=CellDataCDKBWT;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CB_WT=DivisionSizes;
Time_G1S_CB_WT=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CB_WT=DivisionSizes;
Time_Div_CB_WT=tCellCycle;



CellData=CellDataCDKBUE;

t1=1;
t2=2;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_G1S_CB_UE=DivisionSizes;
Time_G1S_CB_UE=tCellCycle;

t1=1;
t2=4;
nCells=numel(CellData);
BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);
for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

Size_Div_CB_UE=DivisionSizes;
Time_Div_CB_UE=tCellCycle;







