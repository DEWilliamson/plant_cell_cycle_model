n=1000;
CdkATs=linspace(0,1,n);

maxlengths=zeros(1,n);
for k=1:n
    [StdSts,Stab]=G2MBifurcation(CdkATs(k));
    maxlengths(k)=numel(StdSts);
end

maxlength=max(maxlengths);
StdSts_=nan(maxlength,n);
Stab_=nan(maxlength,n);

for k=1:n
    [StdSts,Stab]=G2MBifurcation(CdkATs(k));
    m=length(StdSts);
    if m>=1
        StdSts_(1:m,k)=StdSts';
        Stab_(1:m,k)=Stab;
    end
end

CdkATs_=repmat(CdkATs,maxlength,1);

idx=~isnan(StdSts_);
StdSts_=StdSts_(idx);
Stab_=Stab_(idx);
CdkATs_=CdkATs_(idx);

[StdSts_,idx]=sort(StdSts_,'ascend');
CdkATs_=CdkATs_(idx);
Stab_=Stab_(idx);

idx=Stab_>0;
StdSts_u=StdSts_(idx);
CdkATs_u=CdkATs_(idx);

figure(2)
subplot(2,1,2)
plot(CdkATs_,StdSts_,'linewidth',1.2)
hold on
plot(CdkATs_u,StdSts_u,'linewidth',1.2)
legend('Stable','Unstable','interpreter','latex','location','southwest')

xlabel('CDKA/B:CYCB','interpreter','latex')
ylabel('MYB3R3','interpreter','latex')
title('\textbf{B}','interpreter','latex')