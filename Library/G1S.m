function [ts,CdkATs,KRPTs,E2Fs,Rbs]=G1S;

kskrp=0.01; 
kdkrpa=0.01; 
kdkrp=1;
kscdka=0.01;
kdcdka=0.01;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

CdkAT0=0;
KRPT0=1;

y0=[CdkAT0;KRPT0];
tspan=1000;

[ts,ys]=ode45(@model,[0 tspan],y0);
plot(ts,ys)
CdkATs=ys(:,1);
KRPTs=ys(:,2);
[E2Fs,Rbs]=model2(CdkATs,KRPTs);

%tpts=length(ts);
%E2Fs=zeros(tpts,1);
%for k=1:tpts
%    CdkATk=ys(k,1);
%    KRPTk=ys(k,2);
%    E2Fs(k)=model2(CdkATk,KRPTk);
%end
%hold on
%plot(ts,E2Fs)

    function dydt=model(t,y)
        CdkAT=y(1);
        KRPT=y(2);
        
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT*(kprb)*CdkA/((kprb)*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17*E2F)/kd17;
        
        DCdkAT = kscdka - kdcdka*CdkAT;
        DKRPT = kskrp - (kdkrpa + kdkrp*FBL17)*KRPT;
        
        dydt=[DCdkAT;DKRPT];
    end


    function [E2F,Rb]=model2(CdkAT,KRPT)

        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
        
    end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3)^2-4.*arg1.*arg2));
    end
    
end
