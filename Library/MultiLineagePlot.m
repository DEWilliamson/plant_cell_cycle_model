dt=1e-3;

cellNos=numel(CellData);

hold on

l=lines;
c1=l(1,:);
c2=l(2,:);

for cellNo=1:cellNos

    ts=[]; ys=[];
    TS0=[];
    TS=[];
    YS={};
    while cellNo>0

        ts=CellData(cellNo).TS; 
        ys=CellData(cellNo).YS; 
        
        
        TS0=[TS0;ts(1)];
        TS=[TS,{ts}];
        YS=[YS,{ys}];

        cellNo=CellData(cellNo).parent;
    end

    [TS0s,Is]=sort(TS0,'ascend');
    TSs=TS(Is);
    YSs=YS(Is);

    nCL=numel(TSs);

    ts=[];
    Ts=[];
    Ys=[];
    for k=1:nCL
       tsNew=TSs{k}; 

       if ~isempty(ts)
           tsNew(1)=ts(end)+dt;
       end
       ts=tsNew;

       ys=YSs{k};

       Ts=[Ts;ts];
       Ys=[Ys;ys];

    end
    
    plot(Ts,Ys(:,1),'color',[.5 .5 .5])
    %plot(ts(1)+tsPhase,ysPhase,'x','color',c)
end

hold on
for cellNo=1:cellNos
    ts=CellData(cellNo).TS; 
    tsPhase=CellData(cellNo).tsPhase;
    ysPhase=CellData(cellNo).ysPhase;
    plot(tsPhase(2),ysPhase(2),'x','color',c1)    
    plot(tsPhase(4),ysPhase(4),'x','color',c2) 
end


p1=plot(tsPhase(2),ysPhase(2),'x','color',c1);   
p2=plot(tsPhase(4),ysPhase(4),'x','color',c2); 

legend([p1 p2],'G1/S','Division','interpreter','latex','location','southeast')
xlabel('Time','interpreter','latex')
ylabel('Volume','interpreter','latex')